import os
from os.path import isdir, join
from PIL import Image

from torch.utils.data import Dataset

class KittiDataset(Dataset):
  def __init__(self, root_dir, transform=None):
    self.transform = transform

    left_img_dir = join(root_dir, 'image_02/data/')
    self.left_img_fns = [join(left_img_dir, img_fn) for img_fn \
                         in os.listdir(left_img_dir)]

    right_img_dir = join(root_dir, 'image_03/data/')
    self.right_img_fns = [join(right_img_dir, img_fn) for img_fn \
                          in os.listdir(right_img_dir)]

  def __len__(self):
    return len(self.left_img_fns)

  def __getitem__(self, idx):
    left_img = Image.open(self.left_img_fns[idx])
    right_img = Image.open(self.right_img_fns[idx])
    sample = {'left_img': left_img, 'right_img': right_img} 

    if self.transform:
      sample = self.transform(sample)

    return sample

