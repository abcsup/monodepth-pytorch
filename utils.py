import os
import random
from os.path import isdir, join

import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, ConcatDataset

from KittiDataset import KittiDataset

class Resize(object):
  def __init__(self, size):
    self.transform = transforms.Resize(size)

  def __call__(self, sample):
    left_img, right_img = sample['left_img'], sample['right_img']

    return {'left_img': self.transform(left_img),
            'right_img': self.transform(right_img)}

class ToTensor(object):
  def __init__(self):
    self.transform = transforms.ToTensor()

  def __call__(self, sample):
    left_img, right_img = sample['left_img'], sample['right_img']

    return {'left_img': self.transform(left_img),
            'right_img': self.transform(right_img)}

class RandomHorizontalFlip(object):
  def __init__(self):
    self.transform = transforms.RandomHorizontalFlip(p=1)

  def __call__(self, sample):
    left_img, right_img = sample['left_img'], sample['right_img']

    if random.uniform(0, 1) > 0.5:
      return {'left_img': self.transform(right_img),
              'right_img': self.transform(left_img)}
    else:
      return sample

def get_dataloader(root_dir, batch_size, num_workers=4):
  composed = transforms.Compose([Resize((256, 512)),
                                 RandomHorizontalFlip(),
                                 ToTensor()])

  bydate_dirs = [join(root_dir, d) for d in os.listdir(root_dir)]
  bydrive_dirs = []
  for d in bydate_dirs:
    drives = [join(d, drive) for drive in os.listdir(d)]
    bydrive_dirs += drives

  datasets = [KittiDataset(d, transform=composed)
              for d in bydrive_dirs]
  dataset = ConcatDataset(datasets)
  print('Number of images: %d' % len(dataset))

  dataloader = DataLoader(dataset, batch_size=batch_size,
                          shuffle=True, num_workers=num_workers,
                          pin_memory=True)

  return dataloader

