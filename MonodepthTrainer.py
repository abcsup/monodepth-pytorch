import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from os.path import join

from Monodepth import Monodepth

def scale_pyramid(img, num_scales):
  scaled_imgs = [img]
  for i in range(1, num_scales):
    scaled_img = F.interpolate(img, scale_factor=1/2**i, mode='nearest')
    scaled_imgs.append(scaled_img)

  return scaled_imgs

def gradient_x(img):
  img = F.pad(img, (0, 1, 0, 0), mode="replicate")
  return img[:, :, :, :-1] - img[:, :, :, 1:]

def gradient_y(img):
  img = F.pad(img, (0, 0, 0, 1), mode="replicate")
  return img[:, :, :-1, :] - img[:, :, 1:, :]

def get_disp_smoothness(disps, pyramid):
  dx_disps = [gradient_x(d) for d in disps]
  dy_disps = [gradient_y(d) for d in disps]

  dx_pyramid = [gradient_x(img) for img in pyramid]
  dy_pyramid = [gradient_y(img) for img in pyramid]

  weights_x = [torch.exp(-torch.mean(torch.abs(d), dim=1, keepdim=True)) for d in dx_pyramid]
  weights_y = [torch.exp(-torch.mean(torch.abs(d), dim=1, keepdim=True)) for d in dy_pyramid]

  smoothness_x = [dx_disps[i] * weights_x[i] for i in range(4)]
  smoothness_y = [dy_disps[i] * weights_y[i] for i in range(4)]
  smoothness = [torch.abs(smoothness_x[i]) + torch.abs(smoothness_y[i]) for i in range(4)]

  return smoothness

def sampling(img, disp):
  bs, _, height, width = img.size()
  disp = disp[:, 0, :, :]

  x = torch.linspace(0, 1, width).repeat(bs, height, 1).type_as(img)
  y = torch.linspace(0, 1, height).repeat(bs, width, 1).transpose(1,2).type_as(img)
  flow_field = torch.stack((x + disp, y), dim=3)
  
  return F.grid_sample(img, 2 * flow_field - 1)

def generate_left_image(img, left_disp):
  return sampling(img, -left_disp)

def generate_right_image(img, right_disp):
  return sampling(img, right_disp)

def ssim(x, y):
  C1 = 0.01 ** 2
  C2 = 0.03 ** 2

  mu_x = F.avg_pool2d(x, kernel_size=3, stride=1)
  mu_y = F.avg_pool2d(y, kernel_size=3, stride=1)

  sigma_x = F.avg_pool2d(x ** 2, kernel_size=3, stride=1) - mu_x ** 2
  sigma_y = F.avg_pool2d(y ** 2, kernel_size=3, stride=1) - mu_y ** 2
  sigma_xy = F.avg_pool2d(x * y, kernel_size=3, stride=1) - mu_x * mu_y

  ssim_n = (2 * mu_x * mu_y + C1) * (2 * sigma_xy + C2)
  ssim_d = (mu_x ** 2 + mu_y ** 2 + C1) * (sigma_x + sigma_y + C2)

  ssim = ssim_n / ssim_d

  return torch.clamp((1 - ssim) / 2, 0, 1)

class MonodepthTrainer(nn.Module):
  def __init__(self):
    super(MonodepthTrainer, self).__init__()

    self.model = Monodepth()
    self.optim = optim.Adam(self.model.parameters(), lr=1e-4)

    self.ssim_w = 0.85
    self.img_loss_w = 1.0
    self.lr_loss_w = 1.0
    self.disp_loss_w = 0.1
  
  def update(self, left_img, right_img):
    left_pyramid = scale_pyramid(left_img, 4)
    right_pyramid = scale_pyramid(right_img, 4)
    
    disps = self.model(left_img)
    l_disps = [d[:,0,:,:].unsqueeze(1) for d in disps]
    r_disps = [d[:,1,:,:].unsqueeze(1) for d in disps]

    r2l_imgs = [generate_left_image(right_pyramid[i], l_disps[i]) for i in range(4)]
    l2r_imgs = [generate_right_image(left_pyramid[i], r_disps[i]) for i in range(4)]

    r2l_disps = [generate_left_image(r_disps[i], l_disps[i]) for i in range(4)]
    l2r_disps = [generate_right_image(l_disps[i], r_disps[i]) for i in range(4)]

    l_disp_smoothness = get_disp_smoothness(l_disps, left_pyramid)
    r_disp_smoothness = get_disp_smoothness(r_disps, right_pyramid)

    # Loss
    ## L1 loss
    l_l1 = [torch.abs(r2l_imgs[i] - left_pyramid[i]) for i in range(4)]
    l_l1_loss = [torch.mean(l) for l in l_l1]
    r_l1 = [torch.abs(l2r_imgs[i] - right_pyramid[i]) for i in range(4)]
    r_l1_loss = [torch.mean(l) for l in r_l1]
    ## SSIM
    l_ssim = [torch.mean(ssim(r2l_imgs[i], left_pyramid[i])) for i in range(4)]
    r_ssim = [torch.mean(ssim(l2r_imgs[i], right_pyramid[i])) for i in range(4)]
    ## Appearance Matching Loss
    l_img_loss = [self.ssim_w * l_ssim[i] + (1 - self.ssim_w) * l_l1_loss[i] for i in range(4)]
    r_img_loss = [self.ssim_w * r_ssim[i] + (1 - self.ssim_w) * r_l1_loss[i] for i in range(4)]
    img_loss = sum(l_img_loss + r_img_loss)

    ## Disparity Smoothness Loss
    l_disp_loss = [torch.mean(l_disp_smoothness[i]) / 2 ** i for i in range(4)]
    r_disp_loss = [torch.mean(r_disp_smoothness[i]) / 2 ** i for i in range(4)]
    disp_loss = sum(l_disp_loss + r_disp_loss)

    ## Left-Right Consistency
    l_lr_loss = [torch.mean(torch.abs(l_disps[i] - r2l_disps[i])) for i in range(4)]
    r_lr_loss = [torch.mean(torch.abs(r_disps[i] - l2r_disps[i])) for i in range(4)]
    lr_loss = sum(l_lr_loss + r_lr_loss)

    total_loss = img_loss * self.img_loss_w \
                  + disp_loss * self.disp_loss_w \
                  + lr_loss * self.lr_loss_w

    # backprop
    self.optim.zero_grad()
    total_loss.backward()
    self.optim.step()

    self.metrics = {
      'loss/total': total_loss.item(),
      'loss/img': img_loss.item(),
      'loss/smooth': disp_loss.item(),
      'loss/lr': lr_loss.item()
    }

    return l_disps[0]

  def get_metrics(self):
    return self.metrics

  def save(self, save_dir, it):
    data = {
      'model': self.model.state_dict(),
      'optim': self.optim.state_dict(),
      'it': it
    }

    torch.save(data, join(save_dir, 'model.pth'))
    print('Saved at iterations: {}'.format(it))

  def load(self, load_dir):
    data = torch.load(join(load_dir, 'model.pth'))
    self.model.load_state_dict(data['model'])
    self.optim.load_state_dict(data['optim'])
    it = data['it']

    print('Loaded from iterations: {}'.format(it))
    return it

