import argparse
import torch
import torchvision.utils as vutils
from tqdm import tqdm
from tensorboardX import SummaryWriter

from MonodepthTrainer import MonodepthTrainer
from utils import get_dataloader

parser = argparse.ArgumentParser()
parser.add_argument('--batch-size', type=int, default=4)
parser.add_argument('--epochs', type=int, default=10)
parser.add_argument('--resume', action='store_true', default=False)
parser.add_argument('--no-logs', action='store_true', default=False)

use_cuda = torch.cuda.is_available()
DEVICE = torch.device('cuda' if use_cuda else 'cpu')

def to_grid(img, repeat=None):
  if repeat:
    img = img.repeat(1, repeat, 1, 1)
  img = vutils.make_grid(img)
  img_array = img.detach().numpy()

  return img_array

def main(args):
  dataloader = get_dataloader('./kitti', batch_size=args.batch_size)
  writer = SummaryWriter() if not args.no_logs else None

  trainer = MonodepthTrainer().to(DEVICE)

  # resume
  it = 0
  if args.resume:
    it = trainer.load('./weights')

  # training
  for epoch in range(args.epochs):
    print('epoch: {}'.format(epoch))

    for data in tqdm(dataloader):
      left_img, right_img = data['left_img'], data['right_img']

      left_disp = trainer.update(left_img.to(DEVICE), right_img.to(DEVICE))

      # logging
      if writer:
        metrics = trainer.get_metrics()
        for k, v in metrics.items():
          writer.add_scalar(k, v, it)

        if it % 1000 == 0:
          writer.add_image('image', to_grid(left_img), it)
          writer.add_image('disp', to_grid(left_disp, repeat=3), it)

      # save
      if it != 0 and it % 1000 == 0:
        trainer.save('./weights', it)

      it += 1

if __name__ == '__main__':
  args = parser.parse_args()
  main(args)

