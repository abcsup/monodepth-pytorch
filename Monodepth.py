import torch
import torch.nn as nn
import torch.nn.functional as F

def conv3x3(inplanes, outplanes):
  return nn.Conv2d(inplanes, outplanes, kernel_size=3, stride=1, padding=1)

class Conv3x3ELU(nn.Module):
  def __init__(self, inplanes, outplanes):
    super(Conv3x3ELU, self).__init__()
    self.conv = conv3x3(inplanes, outplanes)
    self.activation = nn.ELU(inplace=True)

  def forward(self, x):
    out = self.conv(x)
    out = self.activation(out)

    return out

class ConvDisp(nn.Module):
  def __init__(self, inplanes):
    super(ConvDisp, self).__init__()
    self.conv = conv3x3(inplanes, 2)

  def forward(self, x):
    out = self.conv(x)
    out = torch.sigmoid(out)

    return out * 0.3

class Bottleneck(nn.Module):
  def __init__(self, inplanes, planes, downsample=False):
    super(Bottleneck, self).__init__()
    self.downsample = downsample

    stride = 2 if downsample else 1
    self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, stride=stride)
    self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, padding=1)
    self.conv3a = nn.Conv2d(planes, planes * 4, kernel_size=1)
    self.activation = nn.ELU(inplace=True)

    if downsample:
      self.conv3b = nn.Conv2d(inplanes, planes * 4, kernel_size=1, stride=2)

  def forward(self, x):
    identity = x

    out = self.conv1(x)
    out = self.activation(out)
    
    out = self.conv2(out)
    out = self.activation(out)

    out = self.conv3a(out)

    if self.downsample:
      identity = self.conv3b(identity)

    out += identity
    out = self.activation(out)

    return out

def make_layer(inplanes, planes, num_blocks):
  layers = []
  layers += [Bottleneck(inplanes, planes, downsample=True)]
  for _ in range(1, num_blocks):
    layers += [Bottleneck(planes * 4, planes)]

  return nn.Sequential(*layers)

class UpConv2d(nn.Module):
  def __init__(self, inplanes, outplanes):
    super(UpConv2d, self).__init__()
    self.conv = Conv3x3ELU(inplanes, outplanes)

  def forward(self, x):
    out = F.interpolate(x, scale_factor=2, mode='nearest')
    out = self.conv(out)

    return out

class Monodepth(nn.Module):
  def __init__(self):
    super(Monodepth, self).__init__()
    self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3)
    self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
    # note: original resnet does not downsample at first layer
    self.conv2 = make_layer(64, 64, 3)
    self.conv3 = make_layer(256, 128, 4)
    self.conv4 = make_layer(512, 256, 6)
    self.conv5 = make_layer(1024, 512, 3)

    self.upconv6 = UpConv2d(2048, 512)
    self.iconv6 = Conv3x3ELU(512 + 1024, 512)

    self.upconv5 = UpConv2d(512, 256)
    self.iconv5 = Conv3x3ELU(256 + 512, 256)

    self.upconv4 = UpConv2d(256, 128)
    self.iconv4 = Conv3x3ELU(128 + 256, 128)
    self.disp4 = ConvDisp(128)

    self.upconv3 = UpConv2d(128, 64)
    self.iconv3 = Conv3x3ELU(64 + 64 + 2, 64)
    self.disp3 = ConvDisp(64)

    self.upconv2 = UpConv2d(64, 32)
    self.iconv2 = Conv3x3ELU(32 + 64 + 2, 32)
    self.disp2 = ConvDisp(32)

    self.upconv1 = UpConv2d(32, 16)
    self.iconv1 = Conv3x3ELU(16 + 2, 16)
    self.disp1 = ConvDisp(16)

  def forward(self, x):
    # Encoder
    conv1 = self.conv1(x)
    conv1 = F.elu(conv1, inplace=True)
    pool1 = self.pool1(conv1)

    conv2 = self.conv2(pool1)
    conv3 = self.conv3(conv2)
    conv4 = self.conv4(conv3)
    conv5 = self.conv5(conv4)

    skip1 = conv1
    skip2 = pool1
    skip3 = conv2
    skip4 = conv3
    skip5 = conv4

    # Decoder
    out = self.upconv6(conv5)
    out = torch.cat([out, skip5], 1)
    out = self.iconv6(out)

    out = self.upconv5(out)
    out = torch.cat([out, skip4], 1)
    out = self.iconv5(out)

    out = self.upconv4(out)
    out = torch.cat([out, skip3], 1)
    out = self.iconv4(out)
    disp4 = self.disp4(out)
    udisp4 = F.interpolate(disp4, scale_factor=2, mode='nearest')

    out = self.upconv3(out)
    out = torch.cat([out, skip2, udisp4], 1)
    out = self.iconv3(out)
    disp3 = self.disp3(out)
    udisp3 = F.interpolate(disp3, scale_factor=2, mode='nearest')

    out = self.upconv2(out)
    out = torch.cat([out, skip1, udisp3], 1)
    out = self.iconv2(out)
    disp2 = self.disp2(out)
    udisp2 = F.interpolate(disp2, scale_factor=2, mode='nearest')

    out = self.upconv1(out)
    out = torch.cat([out, udisp2], 1)
    out = self.iconv1(out)
    disp1 = self.disp1(out)

    return disp1, disp2, disp3, disp4

